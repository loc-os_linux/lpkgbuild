# REPOSITORIO DE LPKGBUILD.

Un complemento para instalar paquetes extras en Loc-OS Linux directamente desde el código fuente o sitio oficial del software.

## Contribuir

Puedes contribuir agregando paquetes siguiendo el formato que requiere lpkgbuild.
Los paquetes son tar.gz que incluyen una carpeta con el nombre del programa en minusculas (el mismo nombre que incluirá en el tar.gz) que contienen un script install.sh

Al inicio del script está el she-bang y un comentario indicando quien mantiene el paquete, deben existir tres funciones bash Install(), Remove() y Verify() que son las encargadas de instalar, remover y verificar si está instalado el software
```bash
#!/bin/bash
# Maintainer: Name <email>
Install() {
# Instrucciones de instalación
}

Remove() {
# Instrucciones de desinstalación
}

Verify() {
# Instruciones de verificación del estado
# Se debe exportar la variable STATUS con true o false para indicar si está o no instalado el programa
export STATUS=true
}
```

Al final del script se debe incluir este código para mostrar el mensaje de actualización necesaria en la anterior versión de lpkgbuild ya que no puede usar este nuevo formato
```bash
if ! echo "$0" | grep -q "lpkgbuild"; then
    printf "\e[1;31m%s: \e[0;00m%s\n" "ERROR" "Update lpkgbuild to new version before any install operation"
fi
```

Como lpkgbuild puede saber si usar wget o curl para realizar las descargas, se puede usar una función que provee para realizarlas sin tener que preocuparse por eso
```bash
...
# La función dowload requiere 2 parámetros, el primero es la url de descarga y el segundo es la ruta de destino con el nombre del archivo
download "https://..." "/tmp/filename"
...
```

En el caso de que se instale el programa como un paquete deb, se puede usar la función is_deb_installed() en Verify() para verificar su instalación de forma fácil. Por ejemplo
```bash
Verify() {
    if is_deb_installed "onlyoffice-desktopeditors"; then
        # En el caso de que si esté instalado
        export STATUS=true
    else
        # En el caso de que no esté instalado
        export STATUS=false
    fi
}
```

Algunas pautas a seguir:

- Cualquier archivo fuera de "/tmp/$package" (donde $package es el nombre del paquete) que se descargue en la instación, deberá incluir una instrucción de borrado después de usarse.
- En la remoción se deben borrar repositorios además del software si es que no se han borrado.
- Evitar imprimir información sobre el estado de la instalación, lpkgbuild se encargará de informar esto segun el valor de STATUS
- Disminuir al mínimo la necesidad de intervención del usuario durante la instalación, en el caso de usar apt procurar usar el argumento -y para evitar que se atasque la instalación/remoción
- No usar exit en ningún momento para evitar que se detenga la ejecución de lpkgbuild
- Es recomendable usar shellcheck para mantener buenas prácticas de bash

Puedes ver un ejemplo completo con comentarios explicando en el paquete de [onlyoffice](https://gitlab.com/loc-os_linux/lpkgbuild/-/raw/main/64/onlyoffice.tar.gz)

